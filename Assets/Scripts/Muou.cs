﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muou : MonoBehaviour
{
    private OVRGrabbable _grabbable;
    private GameObject _girl;
    // Start is called before the first frame update
    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
        _girl = GameObject.Find("小女孩");
    }

    // Update is called once per frame
    void Update()
    {
        if (_grabbable.isGrabbed && _girl.GetComponent<GirlAnimationController>().GetProgress() == 2)
        {
            _girl.GetComponent<FollowPath>().StartFollow = true;
            _girl.GetComponent<GirlAnimationController>().SetProgress(3);
        }
    }
}
