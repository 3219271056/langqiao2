﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    private AudioSource audio;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "OVRPlayerController")
        {
            audio.enabled = true;
            if (null != transform.parent.Find("background"))
            {
                transform.parent.Find("background").GetComponent<AudioSource>().enabled = true;
            }
        }
    }
    //
    // private void OnTriggerExit(Collider other)
    // {
    //     if (other.name == "OVRPlayerController")
    //     {
    //         audio.enabled = false;
    //         transform.parent.Find("background").GetComponent<AudioSource>().enabled = false;
    //     }
    // }
}
