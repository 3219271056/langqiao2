﻿using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class SanbeixiangController : MonoBehaviour
{
    public GameObject ui;
    public GameObject video;
    
    private Animator _animator;
    private bool _isFinished;
    private bool _inRange;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.Button.One) && !_isFinished && _inRange)
        {
            video.SetActive(true);
            _animator.Play("倒茶", 0, 0f);
            _isFinished = true;
        }
        
        AnimatorStateInfo info = _animator.GetCurrentAnimatorStateInfo(0);
        // 判断动画是否播放完成
        if (info.IsName("倒茶") && info.normalizedTime >= 1.0f)
        {
            _animator.enabled = false;
        }
    }
    
    void OnTriggerEnter(Collider other){
        if(other.name == "OVRPlayerController")
        {
            _inRange = true;
            ui.SetActive(true);
        }
    }
    
    void OnTriggerExit(Collider other){
        if(other.name == "OVRPlayerController")
        {
            _inRange = false;
            ui.SetActive(false);
            video.SetActive(false);
        }
    }

    void RemoveAnimator()
    {
        GameObject.Find("茶具").GetComponent<Animator>().enabled = false;
    }
}
