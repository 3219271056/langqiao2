﻿using UnityEngine;
using System.Collections;

public class FollowPath : MonoBehaviour {

    public bool StartFollow = false;
    public EditorPathScript PathToFollow;
    public int CurrentWayPointID = 0;
    public float Speed;//移动速度
    public float reachDistance = 0f;//离路径点的最大范围
    public string PathName;//跟随路径的名字
    private string LastName;
    private bool ChangePath = true;
    private bool ChangeState = true;
    private GirlAnimationController _girl;
    
    void Start ()
    {
        _girl = GetComponent<GirlAnimationController>();
    }

    void Update () {
        if (!StartFollow)
            return;
        Speed = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) * 8f + 4f;
        if (ChangeState)
        {
            _girl.Run();
            ChangeState = false;
        }
        if (ChangePath)
        {
            PathToFollow = GameObject.Find(PathName).GetComponent<EditorPathScript>();
            ChangePath = false;
        }
        if (LastName != PathName)
        {
            ChangePath = true;
        }
        LastName = PathName;

        float distance = Vector3.Distance(PathToFollow.path_objs[CurrentWayPointID].position, transform.position);
        //transform.Translate(PathToFollow.path_objs[CurrentWayPointID].position * Time.deltaTime * Speed, Space.Self);
        transform.position = Vector3.MoveTowards(transform.position, PathToFollow.path_objs[CurrentWayPointID].position, Time.deltaTime * Speed);
        transform.LookAt(PathToFollow.path_objs[CurrentWayPointID].position);
        if (distance <= reachDistance)
        {
            CurrentWayPointID++;
        }
        if (CurrentWayPointID >= PathToFollow.path_objs.Count)
        {
            _girl.ClearState();
            StartFollow = false;
            if (_girl.GetProgress() == 3)
            {
                _girl.Clap();
            }
        }
    }
}