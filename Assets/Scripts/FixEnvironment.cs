﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixEnvironment : MonoBehaviour
{
    void Start()
    {
        DynamicGI.UpdateEnvironment();
        RenderSettings.reflectionIntensity = 1.0f;
    }
}
