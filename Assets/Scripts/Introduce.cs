﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Introduce : MonoBehaviour
{
    public GameObject introduce;
    public bool _canRemove;
    private bool _isFinished;
    void OnTriggerEnter(Collider other){
        if(other.name == "GrabVolumeBig" && !_isFinished){
            introduce.SetActive(true);
            _isFinished = true;
        }
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two) && _canRemove)
        {
            introduce.SetActive(false);
        }
    }
}
