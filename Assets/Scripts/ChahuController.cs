﻿using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class ChahuController : MonoBehaviour
{
    private ObiEmitter _emitter;

    private OVRGrabbable _grab;
    // Start is called before the first frame update
    void Start()
    {
        _emitter = GameObject.Find("Emitter").GetComponent<ObiEmitter>();
        _grab = GetComponent<OVRGrabbable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.Button.Two))
        {
            _emitter.KillAll();
        }
        if ((OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger) > 0.8f || OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) > 0.8f) && _grab.isGrabbed)
        {
            _emitter.speed = 4.0f;
        }
        else
        {
            _emitter.speed = 0f;
        }
    }
}
