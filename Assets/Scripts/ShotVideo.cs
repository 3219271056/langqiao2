﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ShotVideo : MonoBehaviour
{
    public float ScreenShotHeight = 500;
    public float ScreenShotWith = 500;
    public string PicSavePath = "MyPhoto";


    [SerializeField]
    private Texture2D picture;
    private RenderTexture renderBuffer;
    private string Path = "";
    private bool IsShot = false;
    private static int count = 0;


    public string RecordPath = "";
    private bool IsRecord = false;
    private static int RecordCount= 0;

    // Use this for initialization
    void Start()
    {
        picture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, false, true);
        Path = Application.dataPath + "/" + PicSavePath;
        //Debug.LogError(Path);
        // SetPath(Path);
        SetPath(RecordPath);

    }

    public void SetPath(string _s)
    {
        //path
        if (Directory.Exists(_s))
        {
            FileAttributes attr = File.GetAttributes(_s);
            if (attr == FileAttributes.Directory)
                Directory.Delete(_s, true);
            else
                File.Delete(_s);
        }
        if (!Directory.Exists(_s))
            Directory.CreateDirectory(_s);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !IsShot)
            ShotScreen();
        if (Input.GetKeyDown(KeyCode.R))
            Record();
        if (Input.GetKeyDown(KeyCode.S))
            StopRecord();

    }

    public bool StartRecord()
    {
        IsRecord = true;
        return IsRecord;
    }
    public bool StopRecord()
    {
        IsRecord = false;
        return IsRecord;
    }

    public void Record()
    {
        StartRecord();
        StartCoroutine(CaptureVideo());
    }

    public void ShotScreen()
    {
        IsShot = true;
        StartCoroutine(CaptureScreen());
    }


    public IEnumerator CaptureScreen()
    {
        yield return new WaitForEndOfFrame();
        picture.ReadPixels(
            new Rect(0, 0, Screen.width, Screen.height), 0, 0
            );

        picture.Apply();
        var bytes = picture.EncodeToPNG();
        File.WriteAllBytes(RecordPath + "/" + count.ToString() + ".png", bytes);
        count++;
        Debug.Log("Fin the work gender texture");

        yield return new WaitForSeconds(0.1f);
        IsShot = false;
    }

    public IEnumerator CaptureVideo()
    {
        while (IsRecord)
        {
            picture.ReadPixels(
          new Rect(0, 0, Screen.width, Screen.height), 0, 0
          );

            picture.Apply();
            var bytes = picture.EncodeToPNG();
            File.WriteAllBytes(RecordPath + "/" + RecordCount.ToString() + ".png", bytes);
            RecordCount++;
            Debug.Log("Fin the work CaptureVideo texture "+ RecordCount);
            yield return new WaitForEndOfFrame();
        }

    }

}