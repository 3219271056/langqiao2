﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public void One()
    {
        Hide();
        // EditorSceneManager.LoadScene("2Xitan");
        SceneManager.LoadScene("2Xitan");
    }
    public void Two()
    {
        Hide();
        // EditorSceneManager.LoadScene("3Jiedao");
        SceneManager.LoadScene("3Jiedao");
    }
    public void Three()
    {
        Hide();
        // EditorSceneManager.LoadScene("4Linshuidian");
        SceneManager.LoadScene("4Linshuidian");
    }

    void Hide()
    {
        GameObject parentGameObject = transform.parent.gameObject;
        parentGameObject.SetActive(false);
    }
}
