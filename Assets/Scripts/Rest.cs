﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rest : MonoBehaviour
{
    public GameObject rain;
    public Material skybox;
    public GameObject ui;
    
    public GameObject options;
    
    public void rest()
    {
        options.SetActive(false);
        rain.SetActive(true);
        ui.SetActive(true);
        Light light = GameObject.FindWithTag("Light").GetComponent<Light>();
        light.color = new Color32(200, 224, 226, 255);
        RenderSettings.reflectionIntensity = 0.6f;
        RenderSettings.ambientIntensity = 0.2f;
        RenderSettings.skybox = skybox;
    }
    
    private void Start()
    {
        
    }
}
