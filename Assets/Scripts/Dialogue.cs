﻿using System;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    private bool _isInRange;
    private AudioSource _audioSource;
    
    public int correctPage;
    public GameObject[] dialogue;
    public AudioClip[] audioClips;

    private void Start()
    {
        if (audioClips.Length != Decimal.Zero)
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.clip = audioClips[0];
            _audioSource.Stop();
        }
    }

    private void Update()
    {
        if (!_isInRange || correctPage >= dialogue.Length)
        {
            Hide();
            return;
        }

        Hide();
        dialogue[correctPage].SetActive(true);
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            correctPage++;

            try
            {
                if (_audioSource && audioClips[correctPage])
                {
                    _audioSource.clip = audioClips[correctPage];
                    _audioSource.Play();
                }
            }
            catch (IndexOutOfRangeException e)
            {
                _audioSource.clip = null;
            }
        }
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.name == "OVRPlayerController" && correctPage == 0){
            if (audioClips.Length != Decimal.Zero)
            {
                _audioSource.Play();
            }
            _isInRange = true;
        }
    }
    
    void OnTriggerExit(Collider other){
        if(other.name == "OVRPlayerController"){
            _isInRange = false;
        }
    }
    
    void Hide()
    {
        foreach (GameObject o in dialogue)
        {
            o.SetActive(false);
        }
    }

}
