﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlAnimationController : MonoBehaviour
{
    private Animator _animator;
    private bool _inRange;
    void Start()
    {
        _animator = GetComponent<Animator>();
        SetProgress(0);
    }

    private void Update()
    {
        if (GetProgress() == 1)
        {
            StartCoroutine(DelayToInvoke.DelayToInvokeDo(() =>
            {
                Talk();
                SetProgress(2);
            }, 2.0f));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "OVRPlayerController")
        {
            _inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "OVRPlayerController")
        {
            _inRange = false;
        }
    }

    public bool InRange()
    {
        return _inRange;
    }

    public void ClearState()
    {
        _animator.SetBool("run", false);
        _animator.SetBool("walk", false);
        _animator.SetBool("cough", false);
        _animator.SetBool("cry", false);
        _animator.SetBool("clap", false);
        _animator.SetBool("talk", false);
    }

    public void Run()
    {
        ClearState();
        _animator.SetBool("run", true);
    }

    public void Walk()
    {
        ClearState();
        _animator.SetBool("walk", true);
    }

    public void Cough()
    {
        ClearState();
        _animator.SetBool("cough", true);
    }

    public void Cry()
    {
        ClearState();
        _animator.SetBool("cry", true);
    }

    public void Clap()
    {
        ClearState();
        _animator.SetBool("clap", true);
    }

    public void Talk()
    {
        ClearState();
        _animator.SetBool("talk", true);
    }

    public void SetProgress(int progress)
    {
        _animator.SetInteger("progress", progress);
    }
    
    public int GetProgress()
    {
        return _animator.GetInteger("progress");
    }
}
