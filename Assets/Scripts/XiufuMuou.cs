﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XiufuMuou : MonoBehaviour
{
    private GirlAnimationController _girlAnimationController;
    private GameObject _girl;
    private Animation _animation;
    private Dialogue _dialogue;
    private bool _isFinished;
    private OVRGrabbable _ovrGrabbable;
    private FollowPath _followPath;
    
    public Text xiufuMuouText;
    
    // Start is called before the first frame update
    void Start()
    {
        _ovrGrabbable = GetComponent<OVRGrabbable>();
        _girl = GameObject.Find("小女孩");
        _girlAnimationController = _girl.GetComponent<GirlAnimationController>();
        _dialogue = _girl.GetComponent<Dialogue>();
        _animation = GetComponent<Animation>();
        _followPath = _girl.GetComponent<FollowPath>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_girlAnimationController.InRange() && _dialogue.correctPage == 1)
        {
            if (OVRInput.Get(OVRInput.Button.Two))
            {
                xiufuMuouText.text = "做的很好！";
                Muou();
            }
        }

        if (_dialogue.correctPage == 2 && !_isFinished)
        {
            _dialogue.correctPage = 1;
        }
        
        if (_ovrGrabbable.isGrabbed && _girlAnimationController.GetProgress() == 2)
        {
            _followPath.StartFollow = true;
            _girlAnimationController.SetProgress(3);
        }
    }
    
    public void Muou()
    {
        _isFinished = true;
        // transform.parent.gameObject.SetActive(false);
        _animation.Play("muou");
        _animation.PlayQueued("CINEMA_4D___");
        AnimationState state = _animation["CINEMA_4D___"];
        state.speed = 0.5f;
        _girlAnimationController.Clap();
        _girlAnimationController.SetProgress(1);
    }
}
