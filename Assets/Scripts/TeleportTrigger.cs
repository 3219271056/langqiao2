﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportTrigger : MonoBehaviour
{
    public GameObject changeScene;
    void OnTriggerEnter(Collider other){
        if(other.name == "OVRPlayerController"){
            changeScene.SetActive(true);
        }
    }
    
    void OnTriggerExit(Collider other){
        if(other.name == "OVRPlayerController"){
            changeScene.SetActive(false);
        }
    }
}