﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitalRuby.RainMaker;
using UnityEngine;

public class MyButton : MonoBehaviour
{
    public GameObject[] gameObjects;
    public GameObject UI;
    public bool canRemove = true;
    
    private void Start()
    {
        
    }
    
    public void SetActive()
    {
        gameObjects[0].SetActive(true);
        UI.transform.Find("options").gameObject.SetActive(false);
    }

    public void Continue()
    {
        gameObjects[0].SetActive(false);
    }
    
    public void Muou()
    {
        // transform.parent.gameObject.SetActive(false);
        Animation anim = GameObject.Find("木偶修复").gameObject.GetComponent<Animation>();
        GirlAnimationController girl = GameObject.Find("小女孩").GetComponent<GirlAnimationController>();
        anim.Play("muou");
        anim.PlayQueued("CINEMA_4D___");
        AnimationState state = anim["CINEMA_4D___"];
        state.speed = 0.5f;
        girl.Clap();
        girl.SetProgress(1);
    }
    
    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two) && canRemove)
        {
            UI.transform.Find("options").gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
