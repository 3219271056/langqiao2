﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuouAnimationController : MonoBehaviour
{
    private Animator _animator;
    private bool _inRange;

    public GameObject ui;
    public GameObject video;
    public AudioSource changxiAudio;
    
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_inRange)
        {
            return;
        }
        changxiAudio.Stop();
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            _animator.SetTrigger("L_arm");
        } else if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            _animator.SetTrigger("R_arm");
        } else if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
        {
            _animator.SetTrigger("L_leg");
        } else if (OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger))
        {
            _animator.SetTrigger("R_leg");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "OVRPlayerController")
        {
            video.SetActive(true);
            ui.SetActive(true);
            _inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.name == "OVRPlayerController")
        {
            video.SetActive(false);
            ui.SetActive(false);
            _inRange = false;
        }
    }
}
