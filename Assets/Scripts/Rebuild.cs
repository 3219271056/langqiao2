﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rebuild : MonoBehaviour
{
    public GameObject[] lingjian;
    public GameObject rebuild;
    public Text text;

    private bool[] _isFinished = new bool[6];

    private void OnTriggerEnter(Collider other)
    {
        String objName = other.name;
        switch (objName)
        {
            case "屋顶":
            {
                lingjian[0].SetActive(true);
                other.gameObject.SetActive(false);
                _isFinished[0] = true;
                break;
            }
            case "桥面":
            {
                lingjian[1].SetActive(true);
                other.gameObject.SetActive(false);
                _isFinished[1] = true;
                break;
            }
            case "桥侧":
            {
                lingjian[2].SetActive(true);
                other.gameObject.SetActive(false);
                _isFinished[2] = true;
                break;
            }
            case "桥下":
            {
                lingjian[3].SetActive(true);
                other.gameObject.SetActive(false);
                _isFinished[3] = true;
                break;
            }
            case "架子":
            {
                lingjian[4].SetActive(true);
                other.gameObject.SetActive(false);
                _isFinished[4] = true;
                break;
            }
            case "屋顶架子":
            {
                lingjian[5].SetActive(true);
                other.gameObject.SetActive(false);
                _isFinished[5] = true;
                break;
            }
        }
    }

    // float m_timer = 0;
    private void Update()
    {
        foreach (bool f in _isFinished)
        {
            if (!f)
            {
                return;
            }
        }

        text.text = "恭喜！\n你完成了廊桥的重建！";
        if (OVRInput.Get(OVRInput.Button.Two))
        {
            rebuild.SetActive(false);
            // rebuild.transform.parent.gameObject.SetActive(true);
        }
    }
}
