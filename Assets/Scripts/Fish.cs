﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Fish : MonoBehaviour
{
    private GameObject[] _fishs;
    private void Start()
    {
        int n = transform.childCount;
        _fishs = new GameObject[n];
        for (int i = 0; i < n; i++)
        { 
            _fishs[i] = transform.GetChild(i).gameObject;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "OVRPlayerController")
        {
            foreach (var fish in _fishs)
            {
                fish.GetComponent<Animation>().Play();
            }
        }
    }
}
